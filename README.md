# ZXSpriter

(or "The program formerly known as ZXEditor")

ZXSpriter is a simple sprite editor that allows to generate sprites in a format compatible with the Sinclair ZX Spectrum.
It is written in Python and uses Gtk3.

It allows to generate any number of sprites and save them as an ASSEMBLER file, compatible with GNU's Z80ASM
( <https://www.nongnu.org/z80asm/> ). The sprites can include a transparency mask and color attributes, if desired.

![Capture of the sprite editor](interface.png)

![Capture of the map editor](interface2.png)

You can install ZXSpriter just by typing

    sudo ./setup.py install

or you can just run it from the folder, without installing, by using

    ./zxspriter.py

(it is intelligent enough to work when launched from another folder too).

## The interface

The interface is divided in four zones:

* at the left, the canvas, where you can paint the sprite itself.

  * Pressing the mouse's left button you will set the pixel to "ink color"
  * Pressing the right button you will set it to "paper color"
  * Pressing the SHIFT key in the keyboard allows to trace a line between the last
    pixel set and the current one (it will be traced to "ink color" or to "paper color",
    depending on which button you pressed when setting the origin).
  * Pressing the middle button will apply the current selector color attributes to that
    character
  * Pressing the middle button with the CONTROL key will copy the attributes under the
    cursor to the panel
  * Pressing the CONTROL key allows to select a rectangular area with the mouse, which
    will be copied to the clipboard as text. After that, it is possible to paste it in
    another place or another sprite by pressing the PASTE or PASTE OR buttons and doing
    click with the mouse left button. Using PASTE will set both the INK and PAPER pixels,
    but using PASTE OR will only set the INK pixels (thus "mixing" the current picture
    with the one being pasted)

* in the middle are all the available operations. It is subdivided in four parts:

  * operations: here you can set the size (in characters, up to 8x8), choose the colors for ink,
       paper, bright and flash (which will be applied to the character below the mouse cursor when the middle button is
       pressed in the mouse), fill a closed zone in the canvas, scroll the sprite in any of the four directions,
       mirror horizontally or flip vertically, and undo or redo (up to 100 operations). The FLASH effect is emulated
       only when the "Emulate flash" is enabled, because it is very distracting while working. The scrolling, by default,
       only scrolls the pixels; but pressing the CONTROL key and any scroll button, will scroll the color attributes.

  * Background picture: sometimes you don't want to create an sprite from scratch, but have a jpg/png/whatever file
       with a picture that you want to transform into an sprite. Here you can select that file with the picture and
       show it under the canvas (adding some transparency to your pixels). It allows you to trace the sprite over the
       picture.

  * Load/Save: here you have a first button to choose the working folder, and immediately below is a text entry where
       you can set the file name which will be used to store these sprites in the specified folder whenever you click the
       "SAVE" button. There is also a "Prefix" text entry where you can set a text that will be appended to the name of
       every sprite here, to avoid collisions when mixing sprites from several files. More details in the "File format"
       section.

  * Mask management: a sprite can optionally have a transparency mask, which is another sprite where transparent
       pixels are set to 1, and opaque ones are set to 0. If the current sprite has mask, here you can set to edit the
       sprite itself, the mask, or show both (when showing both, any edition will be done to the mask). Also, the
       "Create mask" button generates automagically a mask, leaving a 1 pixel border around of the object in the sprite.
       This mask usually is enough in common sprites, but those with big "paper color" zones can require the mask to be
       manually adjusted after.

* at the top-right you have the list of sprites. You can have as many sprites as you want, and each one with its own
  size, colors, mask or not mask... In the list you can edit the name, which, among the prefix, will be used as a
  label in the generated source code, thus allowing you to quickly reference each sprite. Also there are three checks
  that can be modified:

  * size: if this check is set, the first byte of the sprite will contain the size (more info about this in the
       "File format" section). If not, the size will only be specified in a comment in the source code.

  * colors: if this check is set, the color attributes data of this sprite will be stored after the pixel data.
       This allows to choose whether we want to store sprites with specific colors, or store only the pixel data.

  * mask: if this is check, it means that this sprite has a transparency mask, which can be edited and will be
       stored interleaved with the pixel data (more info about this in the "File format" section).

  * interleave colors: if this check is set, the color attributes will be stored interleaved between the pixel data
       to simplify the management. If not, the color attributes will be stored after all the pixel data.

  * map_tile: if this check is set, this sprite can be used for the map

   immediately below are three buttons that allow to add a new, empty sprite to the list, delete an sprite, or clone the
   currently selected sprite (useful for animations). Finally, there is a label showing how much memory (in bytes) are
   consuming that list of sprites. This is very useful, given that each sprite can have its own dimensions, have or not
   mask or attributes, or include or not the size at the beginning.

* finally, at the bottom-right is the Preview zone, where you can see the icon in the right size. You can also adjust
   the zoom level.

The **settings** button, allows to configure the default values for "size", "colors" and "mask" when a new sprite is
created.

The MAP tab is where the map can be edited. It has three main parts:

* The tile list: it shows all the sprites marked as map tiles

* The map editor: it is where the map itself is shown, and where tiles can be put to modify it

* The controls: where it is possible to set the zoom values, the map size and the tile size

Pressing the right button in a tile in the map will set the currently selected tile in that position. Pressing the left
button will set the tile 0, which, by definition, is an empty (a.k.a. "don't paint") tile.

Pressing the button "Force tile size" will cut all the tiles to the specific tile size set. This is because a tile that
is bigger than the tile size, by default will "overflow" up and to the left. Setting this will cut everything that falls
out of the tile size, allowing to see what is "behind".

Pressing the button "Show flag" will show which tiles have set the bit 7. This is because the tile number is only 7 bits,
and the bit 7 can be used as a flag.

There is the option of saving a PNG picture with the current map, by pressing the "Save screenshot" button.

Ctrl+Z, Shift+Ctrl+Z and Ctrl+Y allow to do Undo and Redo.

The "Add a column" and "Add a row" buttons allow to add a column or a row BEFORE the chosen one. Just press the button and
then click in the map where to add a row. The same for "Delete a column" and "Delete a row".

## The file format

Here is a piece of the code generated by ZXSpriter:

```asm
; This file was generated by ZXSpriter
; Do not edit

;sprite_format 3

sprite_a_ball_with_mask:
;has_mask
    DEFB 0xC8 ;size
    DEFB 0xff, 0x00, 0xff, 0x00
    DEFB 0xc3, 0x00, 0x81, 0x00
    DEFB 0x81, 0x18, 0x81, 0x3c
    DEFB 0x81, 0x3c, 0x81, 0x3c
    DEFB 0x81, 0x3c, 0x81, 0x3c
    DEFB 0x81, 0x18, 0x81, 0x3c
    DEFB 0xc3, 0x00, 0x81, 0x00
    DEFB 0xff, 0x00, 0xff, 0x00
;color_attributes
attr_sprite_a_ball_with_mask:
    DEFB 0x38, 0x38

sprite_without_mask_not_attributes:
;map_tile
    ;size 0x01
    DEFB 0x0c
    DEFB 0x03
    DEFB 0xc0
    DEFB 0x30
    DEFB 0x0c
    DEFB 0x03
    DEFB 0xc0
    DEFB 0x30
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00

sprite_without_mask_interleaved_attributes:
    ;size 0x81
    DEFB 0x0c
    DEFB 0x03
    DEFB 0xc0
    DEFB 0x30
    DEFB 0x0c
    DEFB 0x03
    DEFB 0xc0
    DEFB 0x30
    DEFB 0x38 ; color attributes
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x38 ; color attributes

map_array:
;tile_size: 4,4
    DEFB 0x01, 0x24, 0x12
    DEFB 0x3D, 0x11, 0x41
    DEFB 0x3D, 0x11, 0x41
    DEFB 0x3D, 0x11, 0x41
    DEFB 0x3D, 0x11, 0x41


;    DEFW sprite_without_mask_not_attributes

```

This piece contains three sprites: "a_ball_with_mask", "without_mask_not_attributes" and
"without_mask_interleaved_attributes". All of them start with a label where the prefix "sprite" has been added.

The first sprite has a transparency mask. The comment "has_mask" is used by ZXSpriter to detect that when the file is
loaded again. This is why it is not recommended to modify these files, but just to insert them in your code using
an **include** statement. The second lacks that comment, which means that it doesn't have a mask.

Also, the first byte of the "a_ball_with_mask" sprite contains its size and flags. The three lower bits (bits 0-2)
contains the height minus one (since they are all 0, it has a height of one character); the next three (bits 3-5)
contains the width minus one (again, it is 001, which means a width of 2 characters). The bit 6 is a flag specifying if
it has mask or not. The bit 7 set means that it has color attributes.

Meanwhile, the other two sprites doesn't store the size, thus it has the value in a comment to allow ZXSpriter to know
the size and the flags.

Now it comes the pixel data in the common spectrum format: each byte represents eight pixels. But in the "a_ball_with_mask" sprite
we also have the mask information interleaved; this is: each DEFB line is one scanline of the sprite, so, since the sprite
has a width of two characters, we would expect two bytes per DEFB line, but we see four. This is because the first byte
is the mask for the first scanline of the upper-left character of the sprite, and the second byte is the pixel data for
the same piece of the scanline. The third byte is the mask for the first scanline of the upper-second character of the sprite,
and the fourth byte is the pixel data for that same scanline, and so on.

The reason for doing it this way is because it is the most efficient way of painting with masks, because this way, the
code only has to read the current byte from the screen, apply the mask with an AND operation, the, over that result, apply
the pixel data with an OR operation over the result of the previous operation, and store the result again in the screen.

As we can see, the sprite other two sprites don't have mask, and that's why the width is the same than the number of bytes
per DEFB line.

Finally, after the pixel and mask data we have the color attributes, again only in the first sprite, and with the same
label than the whole sprite, but with **attr_** prefixed. The second sprite lacks them because it doesn't have color
attributes. The third sprite, instead, has the color attributes interleaved, stored after the corresponding character
line. Thus in the "attributes interleaved" mode, we have eight scanlines of pixels (which can have, or not have, mask),
and one row with the corresponding color attributes for those characters. After that, we have another eight scanlines
of pixels, and again one row with the color attributes, and so on.

After all the sprites comes (if desired) the map data. It starts with the label *map_array*, and it will have as many
lines as height (in tiles) has the map, and each line will have as many bytes as width (in tiles) has the map.

Finally, after the map comes, commented, a list of the tiles in the precise order. This list can be copied and uncommented
where needed to make the program work. This avoids having to keep track of which sprites are tiles and in which order
are stored.

Reordering the tiles in the sprite editor will modify correctly the tile index in the map, so there is no need of manually
re-compositing the map when changing the position of a tile, only to copy the new list of tiles with the right order.

## Contacting the author

Sergio Costas  
rastersofg@gmail.com  
<http://www.rastersoft.com>
