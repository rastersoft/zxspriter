#!/usr/bin/env python3

import os
import sys
from glob import glob
from distutils.core import setup

try:
    from distutils import dep_util
except:
    pass


def get_data_files():
    data_files = [
        (os.path.join('share', 'applications'), ['zxspriter.desktop']),
        (os.path.join('share', 'icons', 'hicolor', 'scalable', 'apps'), ['zxspriter.svg']),
        (os.path.join('share', 'zxspriter'), glob("*.glade")),
    ]

    for lang_name in [f for f in os.listdir('locale')]:
        mofile = os.path.join('locale', lang_name,
                              'LC_MESSAGES', 'zxspriter.mo')
        # translations must be always in /usr/share because Gtk.builder only
        # search there. If someone knows how to fix this...
        # share/locale/fr/LC_MESSAGES/
        target = os.path.join('/usr', 'share', 'locale',
                              lang_name, 'LC_MESSAGES')
        data_files.append((target, [mofile]))

    return data_files


def compile_translations():

    if (os.system("msgfmt -V > /dev/null") != 0):
        print('You need the binary "msgfmt" (from "gettext") to compile the translations. Aborting')
        sys.exit(-1)

    try:
        for pofile in [f for f in os.listdir('po') if f.endswith('.po')]:
            pofile = os.path.join('po', pofile)

            lang = os.path.basename(pofile)[:-3]  # len('.po') == 3
            # e.g. locale/fr/LC_MESSAGES/
            modir = os.path.join('locale', lang, 'LC_MESSAGES')
            # e.g. locale/fr/LC_MESSAGES/zxspriter.mo
            mofile = os.path.join(modir, 'zxspriter.mo')

            # create an architecture for these locales
            if not os.path.isdir(modir):
                os.makedirs(modir)

            if not os.path.isfile(mofile) or dep_util.newer(pofile, mofile):
                # msgfmt.make(pofile, mofile)
                os.system("msgfmt \"" + pofile + "\" -o \"" + mofile + "\"")
    except:
        pass

try:
    compile_translations()
except:
    print("Failed to generate the translations")


setup(
    name='zxspriter',

    version='0.13',

    description='A ZX Spectrum Sprite editor',
    long_description="A program that allows to create sprites for the ZX Spectrum",

    url='http://www.rastersoft.com',

    author='Raster Software Vigo (Sergio Costas)',
    author_email='raster@rastersoft.com',

    license='GPLv3',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        # 1 - Planning
        # 2 - Pre-Alpha
        # 3 - Alpha
        # 4 - Beta
        # 5 - Production/Stable
        'Development Status :: 4 - Beta',
        'Environment :: X11 Applications :: GTK',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3'
    ],

    keywords='sinclair spectrum',

    packages=[],

    package_dir={},

    #package_data={'zxspriter': ['*.glade']},

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages.
    # see http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    #data_files=[('share/zxspriter/ui', ['ui/test.ui'])],
    data_files=get_data_files(),
    scripts=['zxspriter.py'],
)

if (len(sys.argv) == 2) and (sys.argv[1] == "install"):
    os.system("gtk-update-icon-cache /usr/share/icons/hicolor")
    os.system("gtk-update-icon-cache /usr/local/share/icons/hicolor")
