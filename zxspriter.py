#!/usr/bin/env python3

# Copyright 2021 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of ZXSpriter
#
# ZXSpriter is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# ZXSpriter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import gi
gi.require_version('Gtk', '3.0')
try:
    gi.require_foreign("cairo")
except ImportError:
    print("No pycairo integration :(")
import sys
import os
import gettext
import locale
import traceback
import copy
import configparser
import pathlib
from gi.repository import GLib, Gio, Gtk, Gdk, GObject, GdkPixbuf
import cairo
import pkg_resources

def load_glade(filename):
    if sys.argv[0].startswith("/usr/"):
        if sys.argv[0].startswith("/usr/local/"):
            basepath = "/usr/local/share/zxspriter"
        else:
            basepath = "/usr/share/spriter"
    else:
        basepath = os.path.dirname(sys.argv[0])
    builder = Gtk.Builder()
    builder.add_from_file(os.path.join(basepath, filename))
    return builder

class Settings(object):

    def __init__(self):
        super().__init__()
        self._config = configparser.ConfigParser()
        self._create_default()

        builder = load_glade('settings.glade')
        self._window = builder.get_object('main_window')
        self._store_size = builder.get_object('store_size')
        self._store_colors = builder.get_object('store_colors')
        self._has_mask = builder.get_object('has_mask')
        self._interleaved_colors = builder.get_object('interleaved_colors')
        self._map_tile = builder.get_object('map_tile')

        # read configuration

        config_path = os.path.join(pathlib.Path.home(),'.config','zxspriter')
        try:
            os.makedirs(config_path)
        except:
            pass
        self._config_path = os.path.join(config_path,'zxspriter.ini')
        try:
            self._config.read(self._config_path)
        except:
            self._create_default()

    def get_settings(self):
        values = (self._config['DEFAULT'].getboolean('store_size'),
                  self._config['DEFAULT'].getboolean('store_colors'),
                  self._config['DEFAULT'].getboolean('has_mask'),
                  self._config['DEFAULT'].getboolean('interleaved_colors'),
                  self._config['DEFAULT'].getboolean('map_tile'))
        return values

    def get_zoom(self):
        return self._config['DEFAULT'].getfloat('zoom')

    def set_zoom(self, value):
        self._config['DEFAULT']['zoom'] = f"{value}"
        self.save()

    def get_tile_zoom(self):
        return self._config['DEFAULT'].getfloat('tile_zoom')

    def set_tile_zoom(self, value):
        self._config['DEFAULT']['tile_zoom'] = f"{value}"
        self.save()

    def get_map_zoom(self):
        return self._config['DEFAULT'].getfloat('map_zoom')

    def set_map_zoom(self, value):
        self._config['DEFAULT']['map_zoom'] = f"{value}"
        self.save()

    def get_leave_border(self):
        return self._config['DEFAULT'].getboolean('leave_border_in_mask')

    def set_leave_border(self, value):
        self._config['DEFAULT']['leave_border_in_mask'] = "true" if value else "false"
        self.save()

    def _create_default(self):
        self._config['DEFAULT'] = {'store_size': False,
                                   'store_colors': True,
                                   'has_mask': True,
                                   'interleaved_colors': True,
                                   'map_tile': True,
                                   'leave_border_in_mask': True,
                                   'zoom': '2.0',
                                   'tile_zoom': '2.0',
                                   'map_zoom': '2.0'}

    def save(self):
        try:
            with open(self._config_path, 'w') as configfile:
                self._config.write(configfile)
        except:
            pass

    def show_config(self, top):
        store_size, store_colors, has_mask, interleaved_colors, map_tile = self.get_settings()
        self._store_size.set_active(store_size)
        self._store_colors.set_active(store_colors)
        self._has_mask.set_active(has_mask)
        self._interleaved_colors.set_active(interleaved_colors)
        self._map_tile.set_active(map_tile)
        self._window.show_all()
        value = self._window.run()
        self._window.hide()
        if value == Gtk.ResponseType.OK:
            self._config['DEFAULT']['store_size'] = "True" if self._store_size.get_active() else "False"
            self._config['DEFAULT']['store_colors'] = "True" if self._store_colors.get_active() else "False"
            self._config['DEFAULT']['has_mask'] = "True" if self._has_mask.get_active() else "False"
            self._config['DEFAULT']['interleaved_colors'] = "True" if self._interleaved_colors.get_active() else "False"
            self._config['DEFAULT']['map_tile'] = "True" if self._map_tile.get_active() else "False"
            self.save()


class Sprite(GObject.Object):
    index_counter = 1

    def __init__(self, name, store_size, store_colors, use_mask, interleaved_colors, map_tile):
        super().__init__()
        self.map_index = Sprite.index_counter
        Sprite.index_counter += 1
        self._create_data()
        self.name = name
        self.width = 1
        self.height = 1
        self.store_size = store_size
        self.store_colors = store_colors
        self.use_mask = use_mask
        self.interleaved_colors = interleaved_colors
        self.map_tile = map_tile
        self.fill_mode = "100"
        self._undo_stages = []
        self._redo_stages = []
        self._lastx = 0
        self._lasty = 0
        self._last_color = 1
        # up to 100 UNDO operations
        self._max_undo = 100

    def _tohex(self, value):
        value = hex(value)[2:]
        if len(value) == 1:
            return f"0x0{value}"
        else:
            return f"0x{value}"

    def dup(self, basename):
        sprite = Sprite(basename, self.store_size, self.store_colors, self.use_mask, self.interleaved_colors, self.map_tile)
        sprite.width = self.width
        sprite.height = self.height
        for y in range(128):
            for x in range(128):
                sprite.pixels[y][x] = self.pixels[y][x]
        for y in range(128):
            for x in range(128):
                sprite.mask[y][x] = self.mask[y][x]
        for y in range(16):
            for x in range(16):
                sprite.attrs[y][x] = self.attrs[y][x]
        return sprite

    def save(self, prefix, file):
        if prefix != "":
            prefix += "_"
        file.write(f"{prefix}{self.name}:\n")
        w = self.width - 1
        h = self.height - 1
        size = w * 8 + h
        if self.map_tile:
            file.write(";map_tile\n")
        if self.use_mask:
            file.write(";has_mask\n")
            size += 64
        if self.store_colors:
            size += 128
        if self.store_colors and self.interleaved_colors:
            file.write(";interleaved_colors\n")
        sprite_size = self._tohex(size)
        if not self.store_size:
            file.write(f"    ;size {sprite_size}\n")
        else:
            file.write(f"    DEFB {sprite_size} ;size\n")
        for y1 in range(self.height):
            for y2 in range(8):
                y = y1 * 8 + y2
                file.write("    DEFB ")
                for x1 in range(self.width):
                    acc = 0
                    acc_mask = 0
                    for x2 in range(8):
                        acc *= 2
                        acc_mask *= 2
                        acc += self.pixels[y][x1 * 8 + x2]
                        acc_mask += self.mask[y][x1 * 8 + x2]
                    if x1 != 0:
                        file.write(", ")
                    if self.use_mask:
                        file.write(f"{self._tohex(acc_mask)}, ")
                    file.write(self._tohex(acc))
                file.write("\n")
            if self.store_colors and self.interleaved_colors:
                file.write("    DEFB ")
                for x in range(self.width):
                    if x != 0:
                        file.write(", ")
                    attr = self.attrs[y1][x]
                    acc = attr[0] * 8 + attr[1]
                    if attr[2]:
                        acc += 64
                    if attr[3]:
                        acc += 128
                    file.write(self._tohex(acc))
                file.write("; color attributes\n")
        if self.store_colors and not self.interleaved_colors:
            file.write(";color_attributes\n")
            file.write(f"attr_{prefix}{self.name}:\n")
            for y in range(self.height):
                file.write("    DEFB ")
                for x in range(self.width):
                    if x != 0:
                        file.write(", ")
                    attr = self.attrs[y][x]
                    acc = attr[0] * 8 + attr[1]
                    if attr[2]:
                        acc += 64
                    if attr[3]:
                        acc += 128
                    file.write(self._tohex(acc))
                file.write("\n")
        file.write("\n")

    def get_undo_redo(self):
        return (len(self._undo_stages) != 0), (len(self._redo_stages) != 0)

    def _create_data(self):
        self.pixels = []
        for y in range(128):
            self.pixels.append([0] * 128)
        self.mask = []
        for y in range(128):
            self.mask.append([0] * 128)
        self.attrs = []
        for y in range(16):
            self.attrs.append([(7, 0, False, False)] * 16) # paper, ink, bright, flash

    def _store_stage(self):
        new_pixel = copy.deepcopy(self.pixels)
        new_mask = copy.deepcopy(self.mask)
        new_attrs = copy.deepcopy(self.attrs)
        self._undo_stages.append((new_pixel, new_mask, new_attrs))
        self._redo_stages = []
        if len(self._undo_stages) > self._max_undo:
            self._undo_stages = self._undo_stages[1:]

    def do_undo(self):
        if len(self._undo_stages) != 0:
            self._redo_stages.append((self.pixels, self.mask, self.attrs))
            (self.pixels, self.mask, self.attrs) = self._undo_stages.pop()

    def do_redo(self):
        if len(self._redo_stages) != 0:
            self._undo_stages.append((self.pixels, self.mask, self.attrs))
            (self.pixels, self.mask, self.attrs) = self._redo_stages.pop()

    def mirror(self):
        self._store_stage()
        old_pixels = self.pixels
        old_mask = self.mask
        old_attrs = self.attrs
        self._create_data()
        width = self.width * 8
        for y in range(self.height * 8):
            for x in range(width):
                self.pixels[y][x] = old_pixels[y][width - x - 1]
                self.mask[y][x] = old_mask[y][width - x - 1]
        for y in range(self.height):
            for x in range(self.width):
                self.attrs[y][x] = old_attrs[y][self.width - x - 1]

    def flip(self):
        self._store_stage()
        old_pixels = self.pixels
        old_mask = self.mask
        old_attrs = self.attrs
        self._create_data()
        height = self.height * 8
        for y in range(height):
            for x in range(self.width * 8):
                self.pixels[y][x] = old_pixels[height - y - 1][x]
                self.mask[y][x] = old_mask[height - y - 1][x]
        for y in range(self.height):
            for x in range(self.width):
                self.attrs[y][x] = old_attrs[self.height - y - 1][x]

    def rleft(self, attr):
        self._store_stage()
        old_pixels = self.pixels
        old_mask = self.mask
        old_attrs = self.attrs
        self._create_data()
        width = self.width * 8
        for y in range(self.height * 8):
            for x in range(width):
                if attr:
                    n = x
                else:
                    n = x + 1
                    if n == width:
                        n = 0
                self.pixels[y][x] = old_pixels[y][n]
                self.mask[y][x] = old_mask[y][n]
        for y in range(self.height):
            for x in range(self.width):
                if not attr:
                    n = x
                else:
                    n = x + 1
                    if n == self.width:
                        n = 0
                self.attrs[y][x] = old_attrs[y][n]

    def rright(self, attr):
        self._store_stage()
        old_pixels = self.pixels
        old_mask = self.mask
        old_attrs = self.attrs
        self._create_data()
        width = self.width * 8
        for y in range(self.height * 8):
            for x in range(width):
                if attr:
                    n = x
                else:
                    n = x - 1
                    if n == -1:
                        n = width - 1
                self.pixels[y][x] = old_pixels[y][n]
                self.mask[y][x] = old_mask[y][n]
        for y in range(self.height):
            for x in range(self.width):
                if not attr:
                    n = x
                else:
                    n = x - 1
                    if n == -1:
                        n = self.width - 1
                self.attrs[y][x] = old_attrs[y][n]

    def rdown(self, attr):
        self._store_stage()
        old_pixels = self.pixels
        old_mask = self.mask
        old_attrs = self.attrs
        self._create_data()
        height = self.height * 8
        for y in range(height):
            for x in range(self.width * 8):
                if attr:
                    n = y
                else:
                    n = y - 1
                    if n == -1:
                        n = height - 1
                self.pixels[y][x] = old_pixels[n][x]
                self.mask[y][x] = old_mask[n][x]
        for y in range(self.height):
            for x in range(self.width):
                if not attr:
                    n = y
                else:
                    n = y - 1
                    if n == -1:
                        n = self.height - 1
                self.attrs[y][x] = old_attrs[n][x]

    def rup(self, attr):
        self._store_stage()
        old_pixels = self.pixels
        old_mask = self.mask
        old_attrs = self.attrs
        self._create_data()
        height = self.height * 8
        for y in range(height):
            for x in range(self.width * 8):
                if attr:
                    n = y
                else:
                    n = y + 1
                    if n == height:
                        n = 0
                self.pixels[y][x] = old_pixels[n][x]
                self.mask[y][x] = old_mask[n][x]
        for y in range(self.height):
            for x in range(self.width):
                if not attr:
                    n = y
                else:
                    n = y + 1
                    if n == self.height:
                        n = 0
                self.attrs[y][x] = old_attrs[n][x]

    def _get_color(self, x, y, value, mode):
        if mode == "75":
            if ((x%2) == 1) or ((y%2) == 0):
                color = value
            else:
                color = 1-value
        elif mode == "50":
            if (x%2) == (y%2):
                color = value
            else:
                color = 1-value
        elif mode == "25":
            if ((x%2) == 0) and ((y%2) == 0):
                color = value
            else:
                color = 1-value
        else:
            color = value
        return color

    def fill(self, x, y, in_pixel, value, mode):
        self._store_stage()
        if in_pixel:
            destination = self.pixels
        else:
            destination = self.mask
        pending = [(x,y)]
        painted = []
        while len(pending) != 0:
            (x, y) = pending.pop()
            if destination[y][x] != value:
                destination[y][x] = value
                painted.append((x,y))
                if x > 0:
                    pending.append((x-1,y))
                if x < (self.width * 8 - 1):
                    pending.append((x+1,y))
                if y > 0:
                    pending.append((x,y-1))
                if y < (self.height * 8 - 1):
                    pending.append((x,y+1))

        if mode == "100":
            return
        for x, y in painted:
            destination[y][x] = self._get_color(x, y, value, mode)

    def line(self, x, y):
        points = []
        if (self._lastx == x) and (self._lasty == y):
            return [(x,y)]
        if abs(y-self._lasty) < abs(x-self._lastx):
            if x >= self._lastx:
                x0 = self._lastx
                x1 = x
                y0 = self._lasty
                y1 = y
            else:
                x1 = self._lastx
                x0 = x
                y1 = self._lasty
                y0 = y
            for x in range(x0, x1 + 1):
                y = y0 + (x-x0) * (y1-y0) / (x1-x0)
                points.append((x,int(y + 0.5)))
        else:
            if y >= self._lasty:
                x0 = self._lastx
                x1 = x
                y0 = self._lasty
                y1 = y
            else:
                x1 = self._lastx
                x0 = x
                y1 = self._lasty
                y0 = y
            for y in range(y0, y1 + 1):
                x = x0 + (y-y0) * (x1-x0) / (y1-y0)
                points.append((int(x + 0.5), y))
        return points

    def paint_line(self, x, y, in_pixel, mode):
        self._store_stage()
        points = self.line(x, y)
        self._lastx = x
        self._lasty = y
        for (x,y) in points:
            if in_pixel:
                self.pixels[y][x] = self._get_color(x, y, self._last_color, mode)
            else:
                self.mask[y][x] = self._get_color(x, y, self._last_color, mode)

    def paint(self, x, y, in_pixel, value, mode):
        self._lastx = x
        self._lasty = y
        self._last_color = value
        value = self._get_color(x, y, value, mode)
        if in_pixel:
            if self.pixels[y][x] != value:
                self._store_stage()
                self.pixels[y][x] = value
        else:
            if self.mask[y][x] != value:
                self._store_stage()
                self.mask[y][x] = value

    def set_paste(self, values, x, y, do_or, on_pixels):
        self._store_stage()
        _x = x
        for line in values:
            for value in line:
                if value == "1":
                    if on_pixels:
                        self.pixels[y][x] = 1
                    else:
                        self.mask[y][x] = 1
                else:
                    if not do_or:
                        self.pixels[y][x] = 0
                    else:
                        self.mask[y][x] = 0
                x += 1
            x = _x
            y += 1

    def get_pixel(self, x, y):
        return self.pixels[y][x]

    def get_mask(self, x, y):
        return self.mask[y][x]

    def set_attr(self, x, y, attr):
        if self.attrs[y][x] != attr:
            self._store_stage()
            self.attrs[y][x] = attr

    def get_attr(self, x, y):
        return self.attrs[y][x]


class Ask(object):
    def __init__(self, text):
        super().__init__()
        builder = load_glade('ask.glade')
        self._window = builder.get_object('ask_window')
        label = builder.get_object('text_label')
        label.set_text(text)

    def run(self):
        self._window.show_all()
        value = self._window.run()
        self._window.destroy()
        if value == Gtk.ResponseType.OK:
            return True
        else:
            return False


class Message(object):
    def __init__(self, text):
        super().__init__()
        builder = load_glade('message.glade')
        self._window = builder.get_object('message_window')
        label = builder.get_object('text_label')
        label.set_text(text)

    def run(self):
        self._window.show_all()
        self._window.run()
        self._window.destroy()


class MainWindow(object):
    def __init__(self, application):
        super().__init__()
        self._settings = Settings()

        self._colors = [[0,0,0],[0,0,0.75],[0.75,0,0],[0.75,0,0.75],[0,0.75,0],[0,0.75,0.75],[0.75,0.75,0],[0.75,0.75,0.75],
                        [0,0,0],[0,0,1],[1,0,0],[1,0,1],[0,1,0],[0,1,1],[1,1,0],[1,1,1]]
        self._w = None # Drawing area width
        self._h = None # Drawing area height

        self._tiles_margin = 8
        self._modified = False
        self._zoom = self._settings.get_zoom()
        self._zoom_tile = self._settings.get_tile_zoom()
        self._zoom_map = self._settings.get_map_zoom()
        self._minw = 1
        self._maxw = 3
        self._current_sprite = self._create_new_sprite('sprite')
        self._loading = False
        self._changing = False
        self._swap_flash = False
        self._bg_picture = None
        self._show_bg = False
        self._show_mode = "sprite"
        builder = load_glade("zxspriter.glade")
        self._window = builder.get_object('main_window')
        self._window.set_application(application)
        self._paint = builder.get_object('drawing_area')
        self._preview = builder.get_object('preview')
        self._zoom_adj = builder.get_object('preview_zoom')
        self._zoom_spin = builder.get_object('zoom_spin')
        self._window.add_events(Gdk.EventMask.KEY_PRESS_MASK | Gdk.EventMask.KEY_RELEASE_MASK)
        self._paint.add_events(Gdk.EventMask.BUTTON_PRESS_MASK |Gdk.EventMask.BUTTON_RELEASE_MASK | Gdk.EventMask.POINTER_MOTION_MASK)
        self._adj_h = builder.get_object('adj_height')
        self._adj_w = builder.get_object('adj_width')
        self._combo_ink = builder.get_object('combo_ink')
        self._combo_paper = builder.get_object('combo_paper')
        self._bright = builder.get_object('bright')
        self._flash = builder.get_object('flash')
        self._sprite_list = builder.get_object('sprite_list')
        self._sprite_view = builder.get_object('sprite_view')
        self._emulate_flash = builder.get_object('emulate_flash')
        self._filename = builder.get_object('filename')
        self._sprites_prefix = builder.get_object('sprites_prefix')
        self._filefolder = builder.get_object('filefolder')
        self._edit_sprite = builder.get_object('edit_sprite')
        self._edit_mask = builder.get_object('edit_mask')
        self._edit_both = builder.get_object('edit_both')
        self._gen_mask = builder.get_object('gen_mask')
        self._do_fill = builder.get_object('do_fill')
        self._undo_button = builder.get_object('undo')
        self._redo_button = builder.get_object('redo')
        self._memory_usage = builder.get_object('memory_usage')
        self._fill100 = builder.get_object('fill100')
        self._fill75 = builder.get_object('fill75')
        self._fill50 = builder.get_object('fill50')
        self._fill25 = builder.get_object('fill25')
        self._do_paste = builder.get_object('do_paste')
        self._do_paste_or = builder.get_object('do_paste_or')
        self._filefolder.set_local_only(True)
        self._filefolder.set_uri(f'file://' + os.getcwd())
        self._save_button = builder.get_object('save_button')
        self._leave_border_in_mask = builder.get_object('leave_border_in_mask')
        self._leave_border_in_mask.set_active(self._settings.get_leave_border())
        self._sprite_list_widget = builder.get_object('sprite_list_draw')
        self._tile_zoom_adj = builder.get_object('tile_zoom')
        self._tile_zoom_adj.set_value(float(self._zoom_tile))
        self._map_zoom_adj = builder.get_object('map_zoom')
        self._map_zoom_adj.set_value(float(self._zoom_map))
        self._sprite_list_draw = builder.get_object('sprite_list_draw')
        self._sprite_list_draw.add_events(Gdk.EventMask.BUTTON_PRESS_MASK |Gdk.EventMask.BUTTON_RELEASE_MASK)
        self._map_draw = builder.get_object('map_draw')
        self._map_draw.add_events(Gdk.EventMask.BUTTON_PRESS_MASK |Gdk.EventMask.BUTTON_RELEASE_MASK | Gdk.EventMask.POINTER_MOTION_MASK)
        self._paned_map = builder.get_object('paned_map')
        self._force_tile_size = builder.get_object('force_tile_size')
        self._show_flag = builder.get_object('show_flag')
        self._map_width_value = builder.get_object('map_width')
        self._map_height_value = builder.get_object('map_height')
        self._tile_width_value = builder.get_object('tile_width')
        self._tile_height_value = builder.get_object('tile_height')
        self._coord_label = builder.get_object('label_coords')
        self._save_map = builder.get_object('save_map')
        self._add_row = builder.get_object('add_row')
        self._delete_row = builder.get_object('delete_row')
        self._add_column = builder.get_object('add_column')
        self._delete_column = builder.get_object('delete_column')
        self._show_grid = builder.get_object('show_grid')
        self._updating_add_delete = False

        self._changed_size()
        self._pixel_size = 0
        self._character_size = 0
        self._add_sprite(self._current_sprite)
        self._valid_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_'
        self._zoom_adj.set_value(float(self._zoom))
        self._currx = 0
        self._curry = 0
        self._shift_pressed = False
        self._ctrl_pressed = False
        self._cursor_inside = False
        self._doing_selection = False
        self._selection = None
        self._clipboard = Gtk.Clipboard.get_default(Gdk.Display.get_default())
        self._clipboard.connect("owner_change", self._clipboard_changed)
        builder.connect_signals(self)
        GLib.timeout_add(640, self._do_flash)
        self._load_path = None
        self._clipboard_changed(None, None)
        self._tile_list = []
        self._maptile_list = {}
        self._zoom_tile_map = 1
        self._tile_selected = 0
        self._tile_width = 0
        self._map = None
        self._map_width = 0
        self._map_height = 0
        self._map_tile_width = 4
        self._map_tile_height = 4
        self._map_surface = None
        self._setting_map_size = False
        self._page = 0
        self._map_undo = []
        self._map_redo = []

    def _tohex(self, value):
        value = hex(value)[2:]
        if len(value) == 1:
            return f"0x0{value}"
        else:
            return f"0x{value}"

    # Methods that need modificacion whenever a new option is added to the sprite list

    def _create_new_sprite(self, name = None):
        if name is None:
            name = self._get_free_name('sprite')
        (store_size, store_colors, use_mask, interleaved_colors, map_tile) = self._settings.get_settings()
        return Sprite(name, store_size, store_colors, use_mask, interleaved_colors, map_tile)

    def _add_sprite(self, sprite):
        self._changing = True
        self._sprite_list.append([sprite.name, sprite, True, sprite.store_size, sprite.store_colors, sprite.use_mask, sprite.interleaved_colors, sprite.map_tile])
        nsprites = len(self._get_sprite_list())
        path = Gtk.TreePath.new_from_indices([nsprites - 1])
        self._sprite_view.set_cursor(path, None, False)
        self._changing = False
        self._update_memory_usage()

    def _toggled_save_size(self, widget, path):
        self._update_toggle(path, 3)

    def _toggled_save_colors(self, widget, path):
        self._update_toggle(path, 4)

    def _toggled_use_mask(self, widget, path):
        self._update_toggle(path, 5)

    def _toggled_interleaved_colors(self, widget, path):
        self._update_toggle(path, 6)

    def _toggled_map_tile(self, widget, path):
        self._update_toggle(path, 7)

    def _update_toggle(self, path, idv):
        iter = self._sprite_list.get_iter(path)
        value = not self._sprite_list[iter][idv]
        sprite = self._sprite_list[iter][1]
        if idv == 3:
            sprite.store_size = value
        elif idv == 4:
            sprite.store_colors = value
        elif idv == 5:
            sprite.use_mask = value
        elif idv == 6:
            sprite.interleaved_colors = value
        elif idv == 7:
            sprite.map_tile = value
        self._sprite_list.set(iter, idv, value)
        self._update_memory_usage()

    def _on_load(self, widget):
        if self._modified:
            a = Ask("There are unsaved changes. Load anyway?")
            if not a.run():
                return
        dialog = Gtk.FileChooserDialog()
        dialog.set_action(Gtk.FileChooserAction.OPEN)
        dialog.add_button("Open", 1)
        dialog.add_button("Cancel", 2)
        dialog.set_default_response(1)
        if self._load_path is not None:
            dialog.set_current_folder_uri(self._load_path)
        ffilter = Gtk.FileFilter()
        ffilter.add_pattern("*.asm")
        ffilter.set_name("Asm files")
        dialog.add_filter(ffilter)
        ffilter = Gtk.FileFilter()
        ffilter.add_pattern("*")
        ffilter.set_name("All files")
        dialog.add_filter(ffilter)
        dialog.set_local_only(True)
        retval = dialog.run()
        self._load_path = dialog.get_current_folder_uri()
        dialog.hide()
        if retval == 1:
            self.do_load(dialog.get_uri())

    def do_load(self, uri):
        try:
            if not uri.startswith("file:///"):
                msg = Message(f"The file must be local.")
                msg.run()
                return
            self._loading = True
            self._sprite_list.clear()
            file_name = uri[7:]
            (folder, fname) = os.path.split(file_name)
            with open(file_name, "r") as filedata:
                data = filedata.read().split('\n')
            sprite_format = 1
            current_sprite = None
            prefix = None
            y = 0
            doing_attrs = False
            doing_map = False
            self._map = None
            self._save_map.set_active(False)
            self._map_width = None
            self._map_height = 0
            self._map_tile_width = 4
            self._map_tile_height = 4
            current_line = 0
            for line in data:
                line = line.strip()
                current_line += 1
                if line == '':
                    continue
                if line.startswith(";tile_size: "):
                    s = line[12:].split(",")
                    self._map_tile_width = int(s[0])
                    self._map_tile_height = int(s[1])
                    continue
                if line == 'map_array:':
                    doing_map = True
                    self._save_map.set_active(True)
                    self._map = []
                    continue
                if doing_map:
                    # load map
                    if line.startswith(";"):
                        continue
                    if line.startswith("TILE_INDEX_") and (-1 != line.find(" EQU ")):
                        continue
                    if not line.lower().startswith("defb "):
                        msg = Message(f"The file is not a valid sprite file (invalid map at line {current_line})")
                        msg.run()
                        return
                    data = line[5:].split(",")
                    if self._map_width is None:
                        self._map_width = len(data)
                    else:
                        if self._map_width != len(data):
                            msg = Message(f"The file is not a valid sprite file (incorrect map width at line {current_line})")
                            msg.run()
                            return
                    mapline = []
                    for element in data:
                        mapline.append(int(element.strip()[2:], 16))
                    self._map.append(mapline)
                    self._map_height += 1
                    continue
                if line.startswith(";sprite_format "):
                    sprite_format = int(line[15:].strip())
                    continue
                if line[-1] == ':':
                    if line[:5] == 'attr_':
                        continue
                    if current_sprite is not None:
                        self._add_sprite(current_sprite)
                        self._current_sprite = current_sprite
                        current_sprite = None
                    pos = line.find('_')
                    if pos == -1:
                        prefix = ''
                        name = line[:-1]
                    else:
                        prefix = line[:pos]
                        name = line[pos+1:-1]
                    current_sprite = self._create_new_sprite(name)
                    current_sprite.store_size = False
                    current_sprite.use_mask = False
                    current_sprite.store_colors = False
                    current_sprite.interleaved_colors = False
                    current_sprite.map_tile = False
                    y = 0
                    yc = 0
                    doing_attrs = False
                    continue
                if current_sprite is None:
                    continue
                if line == ";map_tile":
                    current_sprite.map_tile = True
                    continue
                if line == ";has_mask":
                    current_sprite.use_mask = True
                    continue
                if line == ";interleaved_colors":
                    current_sprite.store_colors = True
                    current_sprite.interleaved_colors = True
                    continue
                if -1 != line.find(';size'):
                    if line.lower().startswith('defb '):
                        size = int(line[4:].strip()[:4], 16)
                        current_sprite.store_size = True
                    else:
                        current_sprite.store_size = False
                        size = int(line[5:].strip()[:4], 16)
                    if sprite_format == 1:
                        w = (int(size / 16)) & 7
                        h = (int(size % 16)) & 7
                        current_sprite.width = w + 1
                        current_sprite.height = h + 1
                    elif sprite_format >= 2:
                        w = (int(size / 8)) & 7
                        h = (int(size % 8)) & 7
                        current_sprite.width = w + 1
                        current_sprite.height = h + 1
                    continue
                if line == ';color_attributes':
                    if current_sprite.interleaved_colors:
                        msg = Message(f"The file is not a valid sprite file (found color_attributes in interleaved_color sprite)")
                        msg.run()
                        return
                    doing_attrs = True
                    current_sprite.store_colors = True
                    yc = 0
                    continue
                if line.startswith(";"):
                    continue # jump over comments
                if not line.lower().startswith('defb'):
                    msg = Message(f"The file is not a valid sprite file (doesn't have defb at line {current_line})")
                    msg.run()
                    return
                pos = line.find("; color attributes")
                if pos != -1:
                    line = line[:pos]
                    has_attributes = True
                else:
                    has_attributes = False
                line = line[4:].strip().split(',')
                x = 0
                doing_mask = True
                for d in line:
                    d = d.strip()
                    value = int(d[2:], 16)
                    if doing_attrs or has_attributes:
                        current_sprite.attrs[yc][x] = (int((value & 0x38) / 8),
                                                        int(value & 0x07),
                                                        False if (value & 0x40) == 0 else True,
                                                        False if (value & 0x80) == 0 else True)
                        x += 1
                    else:
                        counter = 128
                        for c in range(8):
                            if value >= counter:
                                if current_sprite.use_mask and doing_mask:
                                    current_sprite.mask[y][x] = 1
                                else:
                                    current_sprite.pixels[y][x] = 1
                                value -= counter
                            else:
                                if current_sprite.use_mask and doing_mask:
                                    current_sprite.mask[y][x] = 0
                                else:
                                    current_sprite.pixels[y][x] = 0
                            x += 1
                            counter = int(counter/2)
                        if current_sprite.use_mask:
                            doing_mask = not doing_mask
                            if not doing_mask:
                                x -= 8
                if doing_attrs or has_attributes:
                    yc += 1
                else:
                    y += 1
            if current_sprite is not None:
                self._add_sprite(current_sprite)
                self._current_sprite = current_sprite
                current_sprite = None
            self._filefolder.set_uri(f'file://' + folder)
            self._filename.set_text(fname)
            self._sprites_prefix.set_text(prefix)
            self._loading = False
            self._select_sprite(None)
            self._do_repaint()
            recent_mgr = Gtk.RecentManager.get_default()
            recent_mgr.add_item(uri)
            if self._map is not None:
                self._setting_map_size = True
                self._map_width_value.set_value(self._map_width)
                self._map_height_value.set_value(self._map_height)
                self._tile_width_value.set_value(self._map_tile_width)
                self._tile_height_value.set_value(self._map_tile_height)
                self._setting_map_size = False
                correspondence = {0: 0}
                counter = 1
                for sprite in self._get_sprite_list():
                    if not sprite.map_tile:
                        continue
                    correspondence[counter] = sprite.map_index
                    counter += 1
                for map_line in self._map:
                    for x in range(self._map_width):
                        i = map_line[x] & 0x7F
                        flag = (map_line[x] & 0x80) != 0
                        if i not in correspondence:
                            map_line[x] = [0, False]
                        else:
                            map_line[x] = [correspondence[i], flag]
        except:
            traceback.print_exc()
            msg = Message(f"There was an error when loading\n\n{file_name}")
            msg.run()
            self._loading = False

    ####

    def _on_leave_border_in_mask_toggled(self, widget):
        self._settings.set_leave_border(widget.get_active())

    def _on_settings(self, widget):
        self._settings.show_config(self._window)

    def _key_changed(self, widget, place):
        is_release = (place.type == Gdk.EventType.KEY_RELEASE)
        if (place.keyval == Gdk.KEY_Shift_L) or (place.keyval == Gdk.KEY_Shift_R):
            self._shift_pressed = not is_release
        if (place.keyval == Gdk.KEY_Control_L) or (place.keyval == Gdk.KEY_Control_R):
            self._ctrl_pressed = not is_release
        if (place.keyval == Gdk.KEY_y) and is_release and (place.state & Gdk.ModifierType.CONTROL_MASK != 0):
            if self._page == 0:
                self._current_sprite.do_redo()
            elif self._page == 1:
                self._do_map_redo()
        elif (place.keyval == Gdk.KEY_Z) and is_release and (place.state & Gdk.ModifierType.CONTROL_MASK != 0):
            if self._page == 0:
                self._current_sprite.do_redo()
            elif self._page == 1:
                self._do_map_redo()
        elif (place.keyval == Gdk.KEY_z) and is_release and (place.state & Gdk.ModifierType.CONTROL_MASK != 0):
            if self._page == 0:
                self._current_sprite.do_undo()
            elif self._page == 1:
                self._do_map_undo()
        self._do_repaint()

    def _do_undo(self, widget):
        self._current_sprite.do_undo()
        self._refresh_undo_redo()
        self._do_repaint()

    def _do_redo(self, widget):
        self._current_sprite.do_redo()
        self._refresh_undo_redo()
        self._do_repaint()

    def _on_zoom_changed(self, widget):
        self._zoom = self._zoom_adj.get_value()
        self._settings.set_zoom(self._zoom)
        self._do_repaint()

    def _on_show_changed(self, widget):
        if self._edit_sprite.get_active():
            self._show_mode = "sprite"
        elif self._edit_mask.get_active():
            self._show_mode = "mask"
        else:
            self._show_mode = "both"
        self._do_repaint()

    def _on_generate_mask(self, widget):
        maxx = self._current_sprite.width * 8
        maxy = self._current_sprite.height * 8
        leave_border = self._leave_border_in_mask.get_active()
        for y1 in range(maxy):
            for x1 in range(maxx):
                if leave_border:
                    do_paint = 1
                    for x2 in range(-1,2):
                        for y2 in range(-1,2):
                            x = x1 + x2
                            y = y1 + y2
                            if (x<0) or (x>=maxx) or (y<0) or (y>=maxy):
                                continue
                            if self._current_sprite.pixels[y][x] == 1:
                                do_paint = 0
                    self._current_sprite.mask[y1][x1] = do_paint
                else:
                    self._current_sprite.mask[y1][x1] = 1 - self._current_sprite.pixels[y1][x1]
        self._do_repaint()

    def _do_flash(self):
        if self._emulate_flash.get_active():
            self._swap_flash = not self._swap_flash
        else:
            self._swap_flash = False
        self._do_repaint()
        return True

    def _on_bg_set(self, widget):
        bg = Gio.File.new_for_uri(widget.get_uri()).get_parse_name()
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(bg)
        self._bg_picture = Gdk.cairo_surface_create_from_pixbuf(pixbuf, 1, None)
        self._bg_w = float(pixbuf.get_width())
        self._bg_h = float(pixbuf.get_height())
        self._do_repaint()

    def _on_bg_toggled(self, widget):
        self._show_bg = widget.get_active()
        self._do_repaint()

    def _on_dup(self, widget):
        sprite = self._get_selected()
        if sprite is not None:
            self._add_sprite(sprite.dup(self._get_free_name(sprite.name)))

    def _on_mirror(self, widget):
        sprite = self._get_selected()
        if sprite is not None:
            sprite.mirror()
            self._do_repaint()

    def _on_flip(self, widget):
        sprite = self._get_selected()
        if sprite is not None:
            sprite.flip()
            self._do_repaint()

    def _on_left(self, widget):
        sprite = self._get_selected()
        if sprite is not None:
            sprite.rleft(self._ctrl_pressed)
            self._do_repaint()

    def _on_right(self, widget):
        sprite = self._get_selected()
        if sprite is not None:
            sprite.rright(self._ctrl_pressed)
            self._do_repaint()

    def _on_up(self, widget):
        sprite = self._get_selected()
        if sprite is not None:
            sprite.rup(self._ctrl_pressed)
            self._do_repaint()

    def _on_down(self, widget):
        sprite = self._get_selected()
        if sprite is not None:
            sprite.rdown(self._ctrl_pressed)
            self._do_repaint()

    def _get_sprite_list(self):
        sprite_list = []
        counter = 0
        while True:
            try:
                iter = self._sprite_list.get_iter(counter)
            except:
                break
            counter += 1
            sprite_list.append(self._sprite_list.get_value(iter, 1))
        return sprite_list

    def _update_memory_usage(self):
        sprites = self._get_sprite_list()
        memory = 0
        for sprite in sprites:
            nbytes = sprite.width * sprite.height * 8
            if sprite.use_mask:
                nbytes *= 2
            if sprite.store_colors:
                nbytes += sprite.width * sprite.height
            if sprite.store_size:
                nbytes += 1
            memory += nbytes
        self._memory_usage.set_label(f"Total memory usage: {memory} bytes")

    def _get_selected(self):
        (path, column) = self._sprite_view.get_cursor()
        if path is None:
            return None
        sprite = self._sprite_list.get_iter(path)
        return self._sprite_list[sprite][1]

    def _remove_invalid_chars(self, string):
        retval = ''
        for c in string:
            if c in self._valid_chars:
                retval += c
        return retval, string == retval

    def _on_prefix_changed(self, widget):
        prefix, valid = self._remove_invalid_chars(self._sprites_prefix.get_chars(0, -1))
        if (prefix.find('_') != -1) or (not valid):
            prefix = prefix.replace('_', '')
            self._sprites_prefix.set_text(prefix)
        if len(prefix) == 0:
            self._save_button.set_sensitive(False)
        else:
            self._save_button.set_sensitive(True)

    def _get_free_name(self, basename):
        while basename[-1] in '0123456789':
            basename = basename[:-1]
        counter = None
        sprite_list = self._get_sprite_list()
        while True:
            found = True
            if counter is None:
                name = basename
                counter = 0
            else:
                name = f"{basename}{counter}"
                counter += 1
            for sprite in sprite_list:
                if name == sprite.name:
                    found = False
                    break
            if found:
                break
        return name

    def _on_leave_canvas(self, widget, event):
        self._cursor_inside = False
        self._do_repaint()

    def _on_add_sprite(self, widget):
        self._add_sprite(self._create_new_sprite())

    def _on_delete_sprite(self, widget):
        (path, column) = self._sprite_view.get_cursor()
        if path is None:
            return None
        sprite = self._sprite_list.get_iter(path)
        if sprite is None:
            return
        ask = Ask(f"Delete the sprite '{self._sprite_list[sprite][1].name}'?")
        if (ask.run()):
            self._sprite_list.remove(sprite)
        self._update_memory_usage()

    def _select_sprite(self, widget):
        if self._loading:
            return
        if self._current_sprite is not None:
            self._current_sprite.fill_mode = self._get_fill_mode()
        self._changing = True
        if widget is not None:
            (model, iter) = widget.get_selection().get_selected()
            if iter is None:
                self._current_sprite = self._create_new_sprite("sprite")
                self._add_sprite(self._current_sprite)
            else:
                self._current_sprite = model[iter][1]
        if self._current_sprite.fill_mode == "100":
            self._fill100.set_active(True)
        elif self._current_sprite.fill_mode == "75":
            self._fill75.set_active(True)
        elif self._current_sprite.fill_mode == "50":
            self._fill50.set_active(True)
        else:
            self._fill25.set_active(True)
        if not self._current_sprite.use_mask:
            self._edit_sprite.set_active(True)
        self._adj_h.set_value(self._current_sprite.height)
        self._adj_w.set_value(self._current_sprite.width)
        self._gen_mask.set_sensitive(self._current_sprite.use_mask)
        self._edit_sprite.set_sensitive(self._current_sprite.use_mask)
        self._edit_mask.set_sensitive(self._current_sprite.use_mask)
        self._edit_both.set_sensitive(self._current_sprite.use_mask)
        self._refresh_undo_redo()
        self._do_repaint()
        self._changing = False

    def _name_edited(self, widget, path, newtext):
        iter = self._sprite_list.get_iter(path)
        newtext, valid = self._remove_invalid_chars(newtext.replace(" ", "_"))
        self._sprite_list[iter][1].name = newtext
        self._sprite_list.set(iter, 0, newtext)

    def _button_pressed(self, widget, place):
        self._shift_pressed = place.state & Gdk.ModifierType.SHIFT_MASK
        self._ctrl_pressed = place.state & Gdk.ModifierType.CONTROL_MASK
        x = int(place.x / self._pixel_size)
        y = int(place.y / self._pixel_size)
        if (self._selection is not None) and (self._do_paste.get_active() or self._do_paste_or.get_active()):
            self._modified = True
            self._current_sprite.set_paste(self._selection, x, y, self._do_paste_or.get_active(), self._show_mode == 'sprite')
            self._do_paste.set_active(False)
            self._do_paste_or.set_active(False)
        elif self._ctrl_pressed and place.button == 1:
            self._init_x = x
            self._init_y = y
            self._doing_selection = True
            self._do_repaint()
        else:
            self._paint_pixel(int(place.x), int(place.y), place.button)

    def _button_released(self, widget, place):
        if self._doing_selection:
            minx = min(self._init_x, self._currx)
            miny = min(self._init_y, self._curry)
            maxx = max(self._init_x, self._currx)
            maxy = max(self._init_y, self._curry)
            selection = "zxspriter_selection\n"
            for y in range(miny,maxy+1):
                for x in range(minx, maxx+1):
                    if self._show_mode == 'sprite':
                        value = self._current_sprite.get_pixel(x,y)
                    else:
                        value = self._current_sprite.get_mask(x,y)
                    selection += "1" if value == 1 else "0"
                selection += '\n'
            self._clipboard.set_text(selection, len(selection))
            self._doing_selection = False
            self._do_repaint()

    def _clipboard_changed(self, widget, event):
        self._clipboard.request_text(self._selection_changed)

    def _selection_changed(self, clipboard, text):
        enable_paste = False
        if (text is not None) and text.startswith("zxspriter_selection\n"):
            lines = text[20:].split("\n")
            height = len(lines)
            if height != 0:
                width = len(lines[0])
                enable_paste = True
                lines2 = []
                for l in lines:
                    if len(l) == 0:
                        continue
                    lines2.append(l)
                    if len(l) != width:
                        enable_paste = False
                        break
        if enable_paste:
            self._selection = lines2
        else:
            self._selection = None
        self._do_paste.set_sensitive(enable_paste)
        self._do_paste.set_active(False)
        self._do_paste_or.set_sensitive(enable_paste)
        self._do_paste_or.set_active(False)

    def _do_paste_toggled(self, widget):
        if self._do_paste.get_active():
            self._do_paste_or.set_active(False)

    def _do_paste_or_toggled(self, widget):
        if self._do_paste_or.get_active():
            self._do_paste.set_active(False)

    def _cursor_moved(self, widget, place):
        self._cursor_inside = True
        self._currx = int(place.x / self._pixel_size)
        self._curry = int(place.y / self._pixel_size)
        self._shift_pressed = place.state & Gdk.ModifierType.SHIFT_MASK
        if (place.state & (Gdk.ModifierType.BUTTON1_MASK | Gdk.ModifierType.BUTTON2_MASK | Gdk.ModifierType.BUTTON3_MASK)):
            if place.state & Gdk.ModifierType.BUTTON1_MASK:
                button = 1
            elif place.state & Gdk.ModifierType.BUTTON2_MASK:
                button = 2
            else:
                button = 3
            if self._ctrl_pressed:
                self._do_repaint()
            else:
                self._paint_pixel(int(place.x), int(place.y), button)
        elif (self._shift_pressed):
            self._do_repaint()

    def _get_fill_mode(self):
        if self._fill100.get_active():
            fillmode = "100"
        elif self._fill75.get_active():
            fillmode = "75"
        elif self._fill50.get_active():
            fillmode = "50"
        else:
            fillmode = "25"
        return fillmode

    def _paint_pixel(self, x, y, button):
        pixelx = int(x / self._pixel_size)
        pixely = int(y / self._pixel_size)
        characterx = int(x / self._character_size)
        charactery = int(y / self._character_size)
        if (characterx >= self._current_sprite.width) or (charactery >= self._current_sprite.height):
            return
        do_fill = self._do_fill.get_active()
        self._do_fill.set_active(False)
        if self._show_mode == 'sprite':
            destination = True
        else:
            destination = False
        fillmode = self._get_fill_mode()
        if button == 1:
            if do_fill:
                self._current_sprite.fill(pixelx, pixely, destination, 1, fillmode)
            else:
                if self._shift_pressed:
                    self._current_sprite.paint_line(pixelx, pixely, destination, fillmode)
                else:
                    self._current_sprite.paint(pixelx, pixely, destination, 1, fillmode)
        elif button == 3:
            if do_fill:
                self._current_sprite.fill(pixelx, pixely, destination, 0, fillmode)
            else:
                if self._shift_pressed:
                    self._current_sprite.paint_line(pixelx, pixely, destination, fillmode)
                else:
                    self._current_sprite.paint(pixelx, pixely, destination, 0, fillmode)
        elif (button == 2) and (self._show_mode != 'mask'):
            if self._ctrl_pressed:
                attr = self._current_sprite.get_attr(characterx, charactery)
                self._combo_paper.set_active(attr[0])
                self._combo_ink.set_active(attr[1])
                self._bright.set_active(attr[2])
                self._flash.set_active(attr[3])
            else:
                self._current_sprite.set_attr(characterx, charactery, (self._combo_paper.get_active(),
                                                                       self._combo_ink.get_active(),
                                                                       self._bright.get_active(),
                                                                       self._flash.get_active()))
        else:
            return
        self._modified = True
        self._refresh_undo_redo()
        self._do_repaint()

    def _refresh_undo_redo(self):
        undo, redo = self._current_sprite.get_undo_redo()
        self._undo_button.set_sensitive(undo)
        self._redo_button.set_sensitive(redo)

    def present(self):
        self._window.present()

    def _resize(self, widget, allocation):
        if ((self._w != allocation.width) or (self._h != allocation.height)):
            self._w = allocation.width
            self._h = allocation.height
            self._do_repaint()
            self._update_memory_usage()

    def _changed_size(self, widget = None):
        if self._loading or self._changing:
            return
        self._current_sprite.height = int(self._adj_h.get_value())
        self._current_sprite.width = int(self._adj_w.get_value())
        self._do_repaint()
        self._update_memory_usage()

    def _on_preview_draw(self, widget, cr):
        show_bg = False
        mini = self._zoom
        mini2 = int(self._zoom / 2)
        cr.set_line_width(1.0)
        for y in range(self._current_sprite.height * 8):
            for x in range(self._current_sprite.width * 8):
                if self._show_mode == 'both':
                    mk = self._current_sprite.mask[y][x]
                    if mk == 1: # transparent
                        cr.set_source_rgb(.5, .5, .5)
                        cr.rectangle(x*mini + 0.5, y*mini + 0.5, mini2, mini2)
                        cr.fill()
                        cr.rectangle((x*mini + mini2) + 0.5, (y*mini + mini2) + 0.5, mini2, mini2)
                        cr.fill()
                        cr.set_source_rgb(1, 1, 1)
                        cr.rectangle((x*mini + mini2) + 0.5, y*mini + 0.5, mini2, mini2)
                        cr.fill()
                        cr.rectangle(x*mini + 0.5, (y*mini + mini2) + 0.5, mini2, mini2)
                        cr.fill()
                        show_bg = False
                    else:
                        show_bg = True
                if (self._show_mode == 'sprite') or ((self._show_mode == 'both') and show_bg):
                    ip = self._current_sprite.pixels[y][x]
                    attrs = self._current_sprite.attrs[int(y/8)][int(x/8)]
                    if self._swap_flash and attrs[3]:
                        ip = 1 - ip
                    color = self._colors[(8 if attrs[2] else 0) + attrs[ip]]
                    cr.set_source_rgb(color[0], color[1], color[2])
                    cr.rectangle(x * self._zoom + 0.5, y * self._zoom + 0.5, self._zoom, self._zoom)
                    cr.fill()
                    cr.rectangle(x * self._zoom + 0.5, y * self._zoom + 0.5, self._zoom, self._zoom)
                    cr.stroke()
                elif self._show_mode == 'mask':
                    ip = self._current_sprite.mask[y][x]
                    if ip == 1:
                        cr.set_source_rgb(0, 0, 0)
                    else:
                        cr.set_source_rgb(1, 1, 1)
                    cr.rectangle(x * self._zoom + 0.5, y * self._zoom + 0.5, self._zoom, self._zoom)
                    cr.fill()
                    cr.rectangle(x * self._zoom + 0.5, y * self._zoom + 0.5, self._zoom, self._zoom)
                    cr.stroke()


    def _do_draw(self, widget, cr):
        base1 = self._w / self._current_sprite.width
        base2 = self._h / self._current_sprite.height
        base = min(base1, base2)
        mini = base / 8
        self._pixel_size = mini
        self._character_size = base

        show_bg = (self._bg_picture is not None) and self._show_bg and self._show_mode == 'sprite'
        if show_bg and self._show_mode == 'sprite':
            cr.save()
            cr.scale(base * self._current_sprite.width / self._bg_w, base * self._current_sprite.height / self._bg_h)
            cr.set_source_surface(self._bg_picture, 0, 0)
            cr.paint()
            cr.restore()

        mini2 = mini / 2

        for y in range(self._current_sprite.height * 8):
            for x in range(self._current_sprite.width * 8):
                show_bg2 = False
                if self._show_mode == 'both':
                    mk = self._current_sprite.mask[y][x]
                    if mk == 1: # transparent
                        cr.set_source_rgb(.5, .5, .5)
                        cr.rectangle(x*mini, y*mini, mini2, mini2)
                        cr.fill()
                        cr.rectangle(x*mini + mini2, y*mini + mini2, mini2, mini2)
                        cr.fill()
                        cr.set_source_rgb(1, 1, 1)
                        cr.rectangle(x*mini + mini2, y*mini, mini2, mini2)
                        cr.fill()
                        cr.rectangle(x*mini, y*mini + mini2, mini2, mini2)
                        cr.fill()
                        show_bg2 = True
                if (self._show_mode == 'sprite') or (self._show_mode == 'both'):
                    ip = self._current_sprite.pixels[y][x]
                    attrs = self._current_sprite.attrs[int(y/8)][int(x/8)]
                    if self._swap_flash and attrs[3]:
                        ip = 1 - ip
                    color = self._colors[(8 if attrs[2] else 0) + attrs[ip]]
                    alpha = 0.75 if show_bg2 else (0.5 if show_bg else 1)
                    cr.set_source_rgba(color[0], color[1], color[2], alpha)
                    cr.rectangle(x*mini, y*mini, mini, mini)
                    cr.fill()
                elif self._show_mode == 'mask':
                    ip = self._current_sprite.mask[y][x]
                    if ip == 1:
                        cr.set_source_rgb(0, 0, 0)
                    else:
                        cr.set_source_rgb(1, 1, 1)
                    cr.rectangle(x*mini, y*mini, mini, mini)
                    cr.fill()

        # Paint the elastic-band for tracing lines
        if self._shift_pressed and not self._do_fill.get_active() and self._cursor_inside:
            line_points = self._current_sprite.line(self._currx, self._curry)
            cr.set_source_rgba(1,0.5,0,0.5)
            for (x,y) in line_points:
                cr.rectangle(x*mini, y*mini, mini, mini)
                cr.fill()

        # Paint the elastic-band for selection
        if self._doing_selection and self._cursor_inside:
            cr.set_source_rgba(1,0.5,0,0.5)
            minx = min(self._init_x, self._currx)
            miny = min(self._init_y, self._curry)
            maxx = max(self._init_x, self._currx)
            maxy = max(self._init_y, self._curry)
            cr.rectangle(minx*mini, miny*mini, (maxx-minx + 1)*mini,(maxy - miny + 1)*mini)
            cr.fill()

        # Paint the grid
        cr.set_source_rgb(0.5,0.5,0.5)
        for yo in range(self._current_sprite.height):
            cr.set_line_width(self._maxw)
            yg = base * yo
            cr.move_to(0, yg)
            cr.line_to(base * self._current_sprite.width, yg)
            cr.stroke()
            cr.set_line_width(self._minw)
            for yi in range(8):
                yf = yg + yi * mini
                cr.move_to(0, yf)
                cr.line_to(base * self._current_sprite.width, yf)
                cr.stroke()

        for xo in range(self._current_sprite.width):
            cr.set_line_width(self._maxw)
            xg = base * xo
            cr.move_to(xg, 0)
            cr.line_to(xg, base * self._current_sprite.height)
            cr.stroke()
            cr.set_line_width(self._minw)
            for xi in range(8):
                xf = xg + xi * mini
                cr.move_to(xf, 0)
                cr.line_to(xf, base * self._current_sprite.height)
                cr.stroke()

    ###### MAP EDITOR #############################################

    def _do_map_undo(self):
        if len(self._map_undo) != 0:
            self._map_redo.append(self._map)
            self._map = self._map_undo.pop()
            self._map_draw.queue_draw()

    def _do_map_redo(self):
        if len(self._map_redo) != 0:
            self._map_undo.append(self._map)
            self._map = self._map_redo.pop()
            self._map_draw.queue_draw()

    def _map_store_undo(self):
        self._map_undo.append(copy.deepcopy(self._map))
        self._map_redo = []
        if len(self._map_undo) > 100:
            del self._map_undo[0]

    def on_force_tile_size_toggled(self, widget):
        self._map_draw.queue_draw()

    def on_show_flag_toggled(self, widget):
        self._map_draw.queue_draw()

    def on_changed_page(self, widget, page, page_num):
        self._page = page_num
        if self._map is None:
            self._map = [[ [0, False] ]]
        self.on_map_size_value_changed(None)
        self.on_tile_value_changed(None)
        self._tile_list = []
        self._maptile_list = {}
        counter = 1
        height = 0
        width = 0

        for sprite in self._get_sprite_list():
            if not sprite.map_tile:
                continue
            w = sprite.width * 8 * self._zoom_tile
            h = sprite.height * 8 * self._zoom_tile
            self._tile_list.append( (counter, sprite, height, height + h + 2*self._tiles_margin ))
            height += h + 2*self._tiles_margin
            if width < w:
                width = w
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(sprite.width * 8 * self._zoom_map), int(sprite.height * 8 * self._zoom_map))
            cr = cairo.Context(surface)
            self._paint_sprite(cr, sprite, self._zoom_map, 0, 0)
            self._maptile_list[sprite.map_index] = (sprite, surface)
            counter += 1
        self._tile_width = width
        self._sprite_list_widget.set_size_request(width, height)
        self._sprite_list_widget.queue_draw()
        self._paned_map.set_position(width + 2*self._tiles_margin + 4)
        self._set_map_size()

    def _set_map_size(self):
        w = self._map_tile_width * 8 * self._zoom_map * self._map_width
        h = self._map_tile_height * 8 * self._zoom_map * self._map_height
        self._map_draw.set_size_request(w, h)
        self._map_draw.queue_draw()

    def _get_tile_coords(self, place):
        x = int(place.x / (8 * self._zoom_map * self._map_tile_width))
        y = int(place.y / (8 * self._zoom_map * self._map_tile_height))
        if (x >= self._map_width) or (y >= self._map_height) or (x<0) or (y<0):
            return None, None
        return x, y

    def on_map_motion(self, widget, place):
        x, y = self._get_tile_coords(place)
        if x is None:
            return
        self._coord_label.set_text(f"{x},{y}")
        if (place.state & (Gdk.ModifierType.BUTTON1_MASK | Gdk.ModifierType.BUTTON3_MASK)):
            if place.state & Gdk.ModifierType.BUTTON1_MASK:
                button = 1
            else:
                button = 3
            self._put_tile(x, y, button)

    def update_add_delete(self, widget):
        # ensure that only one widget is active
        if self._updating_add_delete:
            return
        self._updating_add_delete = True
        status = widget.get_active()
        self._add_row.set_active(False)
        self._delete_row.set_active(False)
        self._add_column.set_active(False)
        self._delete_column.set_active(False)
        widget.set_active(status)
        self._updating_add_delete = False

    def on_map_button_pressed(self, widget, place):
        x, y = self._get_tile_coords(place)
        if x is None:
            return
        self._map_store_undo()
        if self._add_row.get_active():
            self._map.insert(y, [ [0, False] ] * self._map_width)
            self._add_row.set_active(False)
            self._modified = True
            self._map_height += 1
            self._map_height_value.set_value(self._map_height)
            self._map_draw.queue_draw()
            return
        if self._delete_row.get_active():
            del self._map[y]
            self._delete_row.set_active(False)
            self._modified = True
            self._map_height -= 1
            self._map_height_value.set_value(self._map_height)
            self._map_draw.queue_draw()
            return
        if self._add_column.get_active():
            for linea in self._map:
                linea.insert(x, [0, False])
            self._add_column.set_active(False)
            self._modified = True
            self._map_width += 1
            self._map_width_value.set_value(self._map_width)
            self._map_draw.queue_draw()
            return
        if self._delete_column.get_active():
            for linea in self._map:
                del linea[x]
            self._delete_column.set_active(False)
            self._modified = True
            self._map_width -= 1
            self._map_width_value.set_value(self._map_width)
            self._map_draw.queue_draw()
            return

        self._put_tile(x, y, place.button)

    def _put_tile(self, x, y, button):
        self._modified = True
        show_flag = self._show_flag.get_active()
        if button == 1:
            if show_flag:
                self._map[y][x][1] = True
            else:
                self._map[y][x] = [self._tile_selected, False]
        elif button == 3:
            if show_flag:
                self._map[y][x][1] = False
            else:
                self._map[y][x] = (0, False)
        self._map_draw.queue_draw()

    def on_map_size_value_changed(self, widget):
        if self._setting_map_size:
            return
        self._map_width = int(self._map_width_value.get_value())
        self._map_height = int(self._map_height_value.get_value())
        for line in self._map:
            while len(line) < self._map_width:
                line.append( [0, False] )
        while len(self._map) < self._map_height:
            self._map.append([ [0, False] ] * self._map_width)
        self._set_map_size()

    def on_tile_value_changed(self, widget):
        if self._setting_map_size:
            return
        self._map_tile_width = int(self._tile_width_value.get_value())
        self._map_tile_height = int(self._tile_height_value.get_value())
        self._set_map_size()

    def _on_zoom_tile_changed(self, widget):
        self._zoom_tile = self._tile_zoom_adj.get_value()
        self._settings.set_tile_zoom(self._zoom_tile)
        self.on_changed_page(None, None, None)

    def _on_zoom_map_changed(self, widget):
        self._zoom_map = self._map_zoom_adj.get_value()
        self._settings.set_map_zoom(self._zoom_map)
        self.on_changed_page(None, None, None)

    def on_sprite_list_draw(self, widget, cr):
        for (counter, sprite, top, bottom) in self._tile_list:
            if sprite.map_index == self._tile_selected:
                cr.set_source_rgb(0,1,1)
                cr.rectangle(0, top, 2*self._tiles_margin + self._tile_width, bottom-top)
                cr.fill()
            self._paint_sprite(cr, sprite, self._zoom_tile, self._tiles_margin, top+self._tiles_margin)

    def _paint_sprite(self, cr, sprite, zoom, x0, y0):
        w = sprite.width * 8
        h = sprite.height * 8
        for y in range(h):
            for x in range(w):
                ip = sprite.pixels[y][x]
                attrs = sprite.attrs[int(y/8)][int(x/8)]
                if sprite.use_mask:
                    mask = sprite.mask[y][x]
                    if mask == 1:
                        continue
                if self._swap_flash and attrs[3]:
                    ip = 1 - ip
                color = self._colors[(8 if attrs[2] else 0) + attrs[ip]]
                cr.set_source_rgba(color[0], color[1], color[2], 1)
                cr.rectangle(x*zoom + x0, y0+y*zoom, zoom, zoom)
                cr.fill()

    def on_show_grid_toggled(self, widget):
        self._map_draw.queue_draw()

    def on_map_draw(self, widget, cr):
        if self._map is None:
            return
        tile_width = self._map_tile_width * self._zoom_map * 8
        tile_height = self._map_tile_height * self._zoom_map * 8
        force_size = self._force_tile_size.get_active()
        show_flag = self._show_flag.get_active()
        show_grid = self._show_grid.get_active()
        w1 = self._map_tile_width * 4 * self._zoom_map
        h1 = self._map_tile_height * 4 * self._zoom_map
        cr.set_line_width(1.0)
        for y in range(self._map_height):
            for x in range(self._map_width):
                px = x*tile_width
                py = y*tile_height
                index = self._map[y][x][0]
                if index == 0:
                    cr.set_source_rgb(0,0,0)
                    cr.rectangle(px+.5, py+.5, tile_width-1, tile_height-1)
                    cr.stroke()
                    continue
                if index not in self._maptile_list:
                    cr.set_source_rgba(0,0,1,1)
                    cr.rectangle(px, py, w1, h1)
                    cr.fill()
                    cr.rectangle(px+w1, py+h1, w1, h1)
                    cr.fill()
                    cr.set_source_rgba(1,1,0,1)
                    cr.rectangle(px+w1, py, w1, h1)
                    cr.fill()
                    cr.rectangle(px, py+h1, w1, h1)
                    cr.fill()
                    continue
                (sprite, surface) = self._maptile_list[index]
                offset = 0
                if sprite.height > self._map_tile_height:
                    offset = (sprite.height - self._map_tile_height) * 8 * self._zoom_map
                if force_size:
                    height = self._map_tile_height
                    py1 = py
                else:
                    height = sprite.height
                    py1 = py - offset
                cr.set_source_surface(surface, px, py - offset)
                cr.rectangle(px, py1, sprite.width * 8 * self._zoom_map, height * 8 * self._zoom_map)
                cr.fill()
        if show_flag or show_grid:
            for y in range(self._map_height):
                for x in range(self._map_width):
                    cr.set_source_rgb(0.5, 0.5, 0.5)
                    cr.rectangle(x*tile_width, y*tile_height,tile_width, tile_height)
                    cr.stroke()
                    if self._map[y][x][1] and show_flag:
                        px = x*tile_width
                        py = y*tile_height
                        cr.set_source_rgba(.8,0,0,0.5)
                        cr.rectangle(px, py, w1, h1)
                        cr.fill()
                        cr.rectangle(px+w1, py+h1, w1, h1)
                        cr.fill()
                        cr.set_source_rgba(0,.8,0,0.5)
                        cr.rectangle(px+w1, py, w1, h1)
                        cr.fill()
                        cr.rectangle(px, py+h1, w1, h1)
                        cr.fill()


    def on_save_screenshot_clicked(self, widget):
        filech = Gtk.FileChooserDialog(title="Choose a name for the screenshot", parent=self._window, action=Gtk.FileChooserAction.SAVE)
        filech.add_button("Cancel", -1)
        filech.add_button("Save", 0)
        ffilter = Gtk.FileFilter()
        ffilter.add_mime_type("image/png")
        ffilter.set_name("PNG images")
        filech.add_filter(ffilter)
        ffilter = Gtk.FileFilter()
        ffilter.add_pattern("*")
        ffilter.set_name("All files")
        filech.add_filter(ffilter)
        filech.set_local_only(True)
        if filech.run() != 0:
            filech.hide()
            return
        filech.hide()
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(self._map_tile_width * self._zoom_map * 8 * self._map_width), int(self._map_tile_height * self._zoom_map * 8 * self._map_height))
        cr = cairo.Context(surface)
        self.on_map_draw(None, cr)
        fname = filech.get_filename()
        if not fname.lower().endswith(".png"):
            fname += ".png"
        surface.write_to_png(fname)


    def _on_tile_selected(self, widget, place):
        for (counter, sprite, top, bottom) in self._tile_list:
            if (place.y >= top) and (place.y <= bottom):
                self._tile_selected = sprite.map_index
                self._sprite_list_widget.queue_draw()
                return
        self._tile_selected = 0
        self._sprite_list_widget.queue_draw()

    ######### SAVE FILES ################


    def _on_filename_changed(self, widget):
        if len(self._filename.get_chars(0, -1)) == 0:
            self._save_button.set_sensitive(False)
        else:
            self._save_button.set_sensitive(True)

    def _on_save(self, widget):
        name = self._filename.get_chars(0, -1)
        if name[-4:].lower() != ".asm":
            name += ".asm"
        folder = os.path.join(Gio.File.new_for_uri(self._filefolder.get_uri()).get_parse_name(), name)
        if Gio.File.new_for_path(folder).query_exists(None):
            ask = Ask(f"Overwrite this file?\n{folder}")
            if not ask.run():
                return
        self._modified = False
        name = self._sprites_prefix.get_chars(0, -1)
        with open(folder, "w") as file:
            file.write("; This file was generated by ZXSpriter\n; Do not edit\n\n")
            file.write(";sprite_format 4\n\n")
            counter = 1
            correspondence = {0:0}
            for sprite in self._get_sprite_list():
                if sprite.map_tile:
                    correspondence[sprite.map_index] = counter
                    counter += 1
                sprite.save(name, file)
            if self._save_map.get_active():
                file.write("\nmap_array:\n")
                file.write(f";tile_size: {self._map_tile_width},{self._map_tile_height}\n")
                height = 0
                for linea in self._map:
                    height+=1
                    if height>self._map_height:
                        break
                    file.write("    DEFB ")
                    first = True
                    width = 0
                    for element in linea:
                        width+=1
                        if width>self._map_width:
                            break
                        if not first:
                            file.write(", ")
                        first = False
                        if element[0] in correspondence:
                            value = correspondence[element[0]]
                            if element[1]:
                                value += 0x80
                        else:
                            value = 0
                        file.write(self._tohex(value))
                    file.write("\n")
                file.write("\n")
                for sprite in self._get_sprite_list():
                    if not sprite.map_tile:
                        continue
                    if sprite.map_index in correspondence:
                        value = correspondence[sprite.map_index]
                    else:
                        value = 0
                    file.write(f"TILE_INDEX_{sprite.name.upper()} EQU {value}\n")
                file.write(f"\n\n")
                for sprite in self._get_sprite_list():
                    if not sprite.map_tile:
                        continue
                    file.write(f";     DEFW {name}_{sprite.name}\n")
        recent_mgr = Gtk.RecentManager.get_default()
        path = Gio.File.new_for_uri(folder).get_uri()
        recent_mgr.add_item(path)


    def _on_delete_event(self, widget, more):
        if self._modified:
            a = Ask("There are unsaved changes. Quit anyway?")
            if not a.run():
                return True
        return False


    def _do_repaint(self):
        self._paint.queue_draw()
        self._preview.queue_draw()



class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="com.rastersoft.zxspriter", flags=Gio.ApplicationFlags.HANDLES_OPEN, **kwargs)
        GLib.set_application_name("ZXSpriter")
        GLib.set_prgname("zxspriter")
        self._window = None

    def do_startup(self):
        Gtk.Application.do_startup(self)

    def do_activate(self):
        if self._window is None:
            self._window = MainWindow(self)
        self._window.present()

    def do_open(self, file_object, number, extra):
        self.do_activate()
        if number > 0:
            self._window.do_load(file_object[0].get_uri())

Gtk.init(sys.argv)

application = Application()
application.run(sys.argv)
