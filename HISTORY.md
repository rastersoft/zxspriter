# VERSIONS #

* Version 0.13 (2021-03-02)
  * Fixed "Add row"
  * Now stores the map correctly after resizing it
  * Now stores EQU values for the tiles indexes

* Version 0.12 (2021-02-27)
  * Now COPY and PASTE work on masks too
  * Allows to paint a grid over the map

* Version 0.11 (2021-02-20)
  * Added map editor
  * Now can open files from command line
  * Use Control instead of Shift in everything except lines
  * Shift+Ctrl+Z works as REDO

* Version 0.10 (2021-01-26)
  * Now each sprite remembers its fill percentage
  * Now, when painting lines, uses the same color than the one used to select the origin,
    no matter which button is pressed in the end point
  * Allow to do Copy&Paste of blocks
  * Now is shown right in taskbars and docks

* Version 0.9 (2021-01-22)
  * Added installer

* Version 0.8 (2021-01-18)
  * Allows to use dithering for fill and paint
  * Allows to scroll the color attributes
  * Now passing from an sprite with mask to another without mask switches to "show pixels"

* Version 0.7 (2021-01-17)
  * Allows to use Ctrl+Z for undo and Ctrl+Y for redo

* Version 0.6 (2021-01-15)
  * Now doesn't fail when reading version 2 file format
  * Allows the copy the current attributes from a sprite to the controls
  * Now asks when trying to close the program without having saved the sprites

* Version 0.5 (2021-01-14)
  * Changed the size format (but can load old files)

* Version 0.4 (2021-01-12)
  * Allows to store the color attributes interleaved with the pixel data
  * Allows to create the mask without leaving a border between the mask and the sprite
  * Keeps the current colors when showing transparency

* Version 0.3 (2021-01-10)
  * Maximum size set to 8x8 characters
  * Also sets flags to know if it has mask and/or attributes

* Version 0.2 (2021-01-10)
  * Remembers the load path and the last files used
  * Adds a label pointing to the color attributes of each sprite
  * Now remembers the zoom value and the default settings for a new sprite

* Version 0.1 (2021-01-09)
  * first public version
